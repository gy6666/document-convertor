package com.example.documentconvertor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentConvertorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DocumentConvertorApplication.class, args);
    }

}
