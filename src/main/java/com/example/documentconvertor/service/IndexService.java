package com.example.documentconvertor.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface IndexService {

    /**
     * 文件上传
     * @param file 文件
     * @param type 转换方式 1:PDF转WORD 2:PDF转图片 3:WORD转PDF
     * @return
     */
    Map<String, Object> fileUpload(MultipartFile file, String type);

    /**
     * PDF转WORD
     * @param originalName 待转换文档名
     * @return
     */
    Map<String, Object> pdfToWord(String originalName);

    /**
     * WORD转PDF
     * @param originalName
     * @return
     */
    Map<String, Object> wordToPdf(String originalName);

    /**
     * pdf转图片
     * @param originalName
     * @return
     */
    Map<String, Object> pdfToImg(String originalName);

    /**
     * 下载文件
     * @param name
     * @param type
     * @param response
     */
    void download(String name, String type, HttpServletResponse response);

    /**
     * 删除文件
     * @param originalName
     */
    void delete(String originalName);
}
