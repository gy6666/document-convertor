package com.example.documentconvertor.controller;

import com.example.documentconvertor.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private IndexService indexService;

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    /**
     * 文件上传
     * @param file 文件
     * @param type 转换方式 1:PDF转WORD 2:PDF转图片 3:WORD转PDF
     * @return
     */
    @RequestMapping("fileUpload")
    @ResponseBody
    public Map<String,Object> fileUpload(@RequestParam("file") MultipartFile file, @RequestParam("type") String type){
        return indexService.fileUpload(file, type);
    }

    /**
     * 转换
     * */
    @RequestMapping("transform")
    @ResponseBody
    public Map<String,Object> transform(@RequestParam("originalName") String originalName, @RequestParam("type") String type){
        Map<String,Object> result = new HashMap<>();
        result.put("result",false);
        switch (type) {
            case "1":
                result = indexService.pdfToWord(originalName);
                break;
            case "2":
                result = indexService.pdfToImg(originalName);
                break;
            case "3":
                result = indexService.wordToPdf(originalName);
                break;
        }
        return result;
    }

    /**
     * 下载文件
     * @param type 类型
     * @param response
     * @return
     */
    @RequestMapping("download")
    public void download(@RequestParam("name") String name, @RequestParam("type") String type, HttpServletResponse response){
        indexService.download(name, type, response);
    }

    /**
     * 删除文件
     * @param originalName
     */
    @RequestMapping("delete")
    @ResponseBody
    public void delete(@RequestParam("originalName") String originalName){
        indexService.delete(originalName);
    }

}
