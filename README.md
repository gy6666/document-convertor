# document-convertor

#### 介绍
在线文档转换工具

- PDF转WORD
- PDF转图片
- WORD转PDF
![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/152720_86909dde_4950220.png "1622100129(1).png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/152821_4120d086_4950220.png "1622100162(1).png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/152830_ef8451a2_4950220.png "1622100170(1).png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0527/152837_0af62c20_4950220.png "1622100352(1).png")
#### 软件架构
Spring Boot、thymeleaf、jacob、Spire.Pdf、layui

#### 安装教程

1. jacob仅支持Windows服务器，并且服务器需要安装office软件，如WPS Office / Microsoft Office（激活版）/openOffice等

2. 将`jacob-1.17-M2-x64.dll`放到`JAVA_HOME\jre\bin`目录下

3. 安装jacob-1.17到本地maven

    `mvn install:install-file -DgroupId=com.jacob -DartifactId=jacob -Dversion=1.17 -Dpackaging=jar -Dfile="你的路径\jacob-1.17.jar"`

4. Maven带有默认配置来阻止所有HTTP（不安全）存储库，spire.pdf.free的远程仓库为HTTP存储库，通过将此镜像添加到Maven来告诉Maven允许从不安全的存储库下载，在setting中<mirrors>里加入以配置：

   ```xml
   <mirror>
       <id>com.e-iceblue</id>
       <name>e-iceblue</name>
       <url>http://repo.e-iceblue.com/nexus/content/groups/public/</url>
       <mirrorOf>*</mirrorOf>
       <blocked>false</blocked>
   </mirror>
   ```
5.  编译运行
#### 说明

| 功能      | 描述                                                              |
| --------- | ------------------------------------------------------------ |
| PDF转WORD | 使用Spire.Pdf免费版，免费版仅支持转换10页以内，大于10页本系统实行先分割，分割后逐个转换成多个word再采用jacob合并word输出 |
| PDF转图片 | 使用Spire.Pdf免费版，生成图片集打成压缩包                    |
| WORD转PDF | 使用jacob转换                                                |

